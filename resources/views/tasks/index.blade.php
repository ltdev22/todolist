@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <div class="panel-body">

                        @include('partials.errors')

                        <form action="{{ route('tasks.store') }}" method="post" class="form-horizontal">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="name" class="col-sm-3 control-label">Task:</label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="name" id="name">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <button class="btn btn-primary btn-block" type="submit">Add Task</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
        
        @include('tasks.tasks')
       
    </div>

@endsection
