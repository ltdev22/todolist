<div class="row">
    <div class="col-md-8 col-md-offset-2">
        @if($tasks->count())
            <div class="panel panel-default">
                <div class="panel-heading">Current Tasks</div>

                <div class="panel-body">

                    <table class="table table-striped">
                        <thead>
                            <th>Tasks</th>
                            <th>&nbsp;</th>
                        </thead>
                        <tbody>
                            @foreach($tasks as $task)
                                <tr>
                                    <td>{{ $task->name }}</td>
                                    <td>
                                        <form action="{{ route('tasks.destroy', $task->id) }}" method="post">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        @else
            <div class="alert alert-info">
                You have no tasks at the moment. Start by creating some.
            </div>  
        @endif
    </div>
</div>