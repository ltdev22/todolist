# TODO List #

This is a simple TODO list developed in [Laravel PHP framework](https://laravel.com).

### About the app ###

* Laravel v5.4
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Do `git pull`
* Set up the database
* Run `php artisan migrate`