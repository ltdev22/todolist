<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use App\Repositories\TaskRepo;

class TasksController extends Controller
{
    protected $tasks;
    /**
     * Create a new controller instance.
     * Redirect user to login page if not signed in
     *
     * @return void
     */
    public function __construct(TaskRepo $tasks)
    {
        $this->middleware('auth');
        $this->tasks = $tasks;
    }

    /**
     * Display a list of task
     *
     */
    public function index()
    {
        $tasks = $this->tasks->forUser(auth()->user());

        return view('tasks.index')->with('tasks', $tasks);
    }

    /**
     * Create a new task
     *
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);

        $request->user()->tasks()->create([
            'name' => $request->name,
        ]);

        return redirect()->back();
    }

    /**
     * Delete a task
     *
     */
    public function destroy(Task $task)
    {
        $this->authorize('destroy', $task);

        $task->delete();

        return redirect()->back();
    }
}
