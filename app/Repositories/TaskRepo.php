<?php 

namespace App\Repositories;

use App\User;

class TaskRepo
{
    /**
     * Grab a task for user
     */
    public function forUser(User $user)
    {
        return $user->tasks()->latest()->get();
    }
}